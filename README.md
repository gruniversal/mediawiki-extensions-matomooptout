MatomoOptOut extension for MediaWiki
====================================

This extension integrates the open source analytics alternative [Matomo](https://matomo.org/) (formerly known as Piwik) into your MediaWiki application.

* Version 4.4.2
* Last updated: 17th May 2022
* License: GNU GPL v2+

This extension was started as part of a [bigger project](https://gitlab.com/gruniversal/erwin) and was moved to a [separate repo](https://gitlab.wikimedia.org/gruniversal/mediawiki-extensions-matomooptout) in February 2023 to make it easier for everyone to use it and to allow for further development.

Since I'm not very active in mediawiki development, please feel free to support this extension or contact me, if you want to maintain it: David Gruner (gruniversal@gmx.de).

### Credits

This extension is a modified version of the regular [Matomo extension](https://www.mediawiki.org/wiki/Extension:Matomo).

It has been developed based on version 4.0.1 from the original source code:
https://github.com/DaSchTour/matomo-mediawiki-extension/

To make it distinguishable from the original extension and since it is further developed separately the extension has been renamed to "MatomoOptOut".

The added term "OptOut" indicates, that it comes with a simple opt-out checkbox to allow users to sign off from being tracked.

This version also fixes some problems in the original version as described here:
https://github.com/DaSchTour/matomo-mediawiki-extension/issues/31

This extension should work as a complete replacement for the original Matomo extension.

Also thanks to Valerio Bozzolan for the idea to separate the repo and move it to WikiMedias GitLab: https://phabricator.wikimedia.org/T329293

## Installation

### Requirements

The extension requires at least the following versions to fully work:

* MediaWiki 1.25+
* Matomo 2.7+

### Setup Matomo

This extension does not contain Matomo, so you have to install and setup it separately first.

If you haven't set up a site for your MediaWiki yet, follow this guide to add a new site to Matomo: https://matomo.org/docs/manage-websites/

Be sure to note URL and site ID, which are needed during installation.

### Install extension and basic configuration

Download this extension into the extensions directory of your local MediaWiki instance.

Then enable it in `LocalSettings.php` by adding the following at the end of the file:

  ```php
  wfLoadExtension( 'MatomoOptOut' );
  $wgMatomoURL = "https://your-matomo-server.tld/matomo/matomo.php";
  $wgMatomoIDSite = 1;
  ```

Fill in your URL (`$wgMatomoURL`) and site ID (`$wgMatomoIDSite`) for your site based on your specific Matomo setup.

Note: Until version 4.2.0 `$wgMatomoURL` had to be defined without protocol and filename (e.g. `"matomo-host.tld/dir/"`). This configuration will still work but is deprecated.

Check if the MatomoOptOut extension is loaded in MediaWiki. It should be listed on the page "Special:Version".

### Matomo opt-out checkbox

This extension offers a simple way to include an opt out mechanism into your pages.

You only need to add this parser tag to your page (e.g. data protection):

  ```html
  <matomo-optout />
  ```

User can then toggle their consent status by clicking on the corresponding text.

This can replace the none-responsive iframe opt-out and is inspired by this article:
https://developer.matomo.org/guides/tracking-javascript-guide#optional-creating-a-custom-opt-out-form

### Custom configuration

Disable cookies permanent cookies (default: `false`)

  ```php
  $wgMatomoDisableCookies = true;
  ```

Do not track regular editors (default: `false`)

  ```php
  $wgMatomoIgnoreEditors = true;
  ```

Do not track bots (default: `true`)

  ```php
  $wgMatomoIgnoreEditors = true;
  ```

Do not track sysop users (default: `true`)

  ```php
  $wgMatomoIgnoreSysops = true;
  ```

Track users by their MediaWiki username:  (default: `false`)

  ```php
  $wgMatomoTrackUsernames = true;
  ```

You may also add custom javascript callbacks to the inserted Matomo code:

  ```php
  $wgMatomoCustomJS = "_paq.push(['trackGoal', '1']);"
  ```

  If you have multiple variables to define, use an array:

  ```php
  $wgMatomoCustomJS = array (
    "_paq.push(['setCustomVariable', '1', 'environment', 'production']);",
    "_paq.push(['setCustomVariable', '1', 'is_user', 'yes']);"
  );
  ```


